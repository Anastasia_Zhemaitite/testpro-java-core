public class ExerciseLoops {

    public static void main(String[] args) {
        String text = "What the exercise!";
        int textLength = text.length();

        if (textLength % 2 == 0) {
            System.out.println("The string is even.");
        }else
            System.out.println("The string is odd.");
    }

}
