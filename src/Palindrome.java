import java.util.*;

public class Palindrome {

    public static void main(String args[]) {
        String first, reverse = "";
        first = "Racecar";

        int length = first.length();

        for (int i = length - 1; i >= 0; i--)
            reverse = reverse + first.charAt(i);

        if (first.equalsIgnoreCase(reverse))
            System.out.println("The string is a palindrome.");
        else
            System.out.println("The string isn't a palindrome.");

    }



}
